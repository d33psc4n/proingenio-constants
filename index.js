const { fileUploadFields, fileUploadActions, fileMetadataFields } = require('./app/fileUpload')
const { schoolFields, schoolActions } = require('./app/school')
const { userFields, userActions } = require('./user/user')
const { roleFields, roleActions } = require('./user/role')
const { chatFields, unreadChatFields, messageFields, chatActions } = require('./chat')
const { roomFields, meetingActions, zoomRoomFields, meetingServerFields } = require('./meeting')
const {
  notificationClassFields,
  notificationFields,
  notificationRouteFields,
  fcmTokenFields,
  notificationActions,
  notificationClasses,
} = require('./app/notification')
const { feedbackFields, feedbackActions } = require('./app/feedback')
const { attendanceFields, attendanceActions } = require('./class/attendance')
const { courseFeedbackActions, courseFeedbackFields } = require('./class/courseFeedback')
const { gradeFields, gradeTypeFields, gradeActions, averageOverrideFields } = require('./class/grade')
const {
  quizBlankItemFields,
  quizChoiceOptionFields,
  quizDropdownItemFields,
  quizFields,
  quizItemFields,
  quizItemTypeFields,
  quizItemTypes,
  quizQuestionFields,
  quizStatusFields,
  quizStatuses,
  quizTeacherActions,
} = require('./quiz/teacher')

const {
  quizBlankAnswerFields,
  quizDropdownAnswerFields,
  quizStudentActions,
  quizTakeFields,
  quizTextAnswerFields,
  quizUploadAnswerFields,
} = require('./quiz/student')

const {
  assignmentActions,
  assignmentFeedbackFields,
  assignmentFields,
  assignmentSubmissionFields,
} = require('./class/assignment')
const { subjectFields, courseFields, courseActions, classWorkFields } = require('./class/course')
const { announcementFields, announcementActions } = require('./class/announcement')
const { postFields, postActions } = require('./class/post')
const { classRoomFields, classRoomActions } = require('./class/classRoom')
const { timetableItemFields, timetableFields, timetableActions } = require('./class/timetable')

const generalFields = {
  createdAt: 'createdAt',
  fileUploads: 'fileUploads',
  objectId: 'objectId',
  school: 'school',
  updatedAt: 'updatedAt',
}

const fields = {
  announcementFields,
  assignmentFeedbackFields,
  assignmentFields,
  assignmentSubmissionFields,
  attendanceFields,
  chatFields,
  classRoomFields,
  courseFeedbackFields,
  classWorkFields,
  courseFields,
  fcmTokenFields,
  feedbackFields,
  fileMetadataFields,
  fileUploadFields,
  generalFields,
  averageOverrideFields,
  gradeFields,
  gradeTypeFields,
  meetingServerFields,
  messageFields,
  notificationClassFields,
  notificationFields,
  notificationRouteFields,
  postFields,
  quizBlankAnswerFields,
  quizBlankItemFields,
  quizChoiceOptionFields,
  quizDropdownAnswerFields,
  quizDropdownItemFields,
  quizFields,
  quizItemFields,
  quizItemTypeFields,
  quizQuestionFields,
  quizStatusFields,
  quizStatuses,
  quizTakeFields,
  quizTextAnswerFields,
  quizUploadAnswerFields,
  roleFields,
  roomFields,
  schoolFields,
  subjectFields,
  timetableFields,
  timetableItemFields,
  unreadChatFields,
  userFields,
  zoomRoomFields,
}

const actions = {
  ...announcementActions,
  ...assignmentActions,
  ...attendanceActions,
  ...chatActions,
  ...courseFeedbackActions,
  ...classRoomActions,
  ...courseActions,
  ...feedbackActions,
  ...fileUploadActions,
  ...gradeActions,
  ...meetingActions,
  ...notificationActions,
  ...postActions,
  ...quizStudentActions,
  ...quizTeacherActions,
  ...roleActions,
  ...schoolActions,
  ...timetableActions,
  ...userActions,
}

const tableNames = {
  announcement: 'Announcement',
  assignment: 'Assignment',
  averageOverride: 'AverageOverride',
  assignmentFeedback: 'AssignmentFeedback',
  assignmentSubmission: 'AssignmentSubmission',
  attendance: 'Attendance',
  chat: 'Chat',
  classRoom: 'ClassRoom',
  course: 'Course',
  fcmToken: 'FCMToken',
  classWork: 'ClassWork',
  feedback: 'Feedback',
  fileMetadata: 'FileMetadata',
  fileUpload: 'FileUpload',
  grade: 'Grade',
  gradeType: 'GradeType',
  meetingServer: 'MeetingServer',
  message: 'Message',
  notification: 'Notification',
  notificationClass: 'NotificationClass',
  notificationRoute: 'NotificationRoute',
  post: 'Post',
  quiz: 'Quiz',
  quizBlankAnswer: 'QuizBlankAnswer',
  quizBlankItem: 'QuizBlankItem',
  courseFeedback: 'CourseFeedback',
  quizChoiceOption: 'QuizChoiceOption',
  quizDropdownAnswer: 'QuizDropdownAnswer',
  quizDropdownItem: 'QuizDropdownItem',
  quizItem: 'QuizItem',
  quizItemType: 'QuizItemType',
  quizQuestion: 'QuizQuestion',
  quizStatus: 'QuizStatus',
  quizTake: 'QuizTake',
  quizTextAnswer: 'QuizTextAnswer',
  quizUploadAnswer: 'QuizUploadAnswer',
  role: '_Role',
  room: 'Room',
  schoolData: 'SchoolData',
  subject: 'Subject',
  timetable: 'Timetable',
  timetableItem: 'TimetableItem',
  unreadChat: 'UnreadChat',
  user: '_User',
  zoomRoom: 'ZoomRoom',
}

const primaryRoleNames = {
  administrator: 'Administrator',
  classMaster: 'ClassMaster',
  parent: 'Parent',
  secretary: 'Secretary',
  student: 'Child',
  teacher: 'Teacher',
}

const getOptions = (context) => ({
  useMasterKey: true,
  context,
})

const masterObj = { useMasterKey: true }

const errors = {
  notAllowed: 'Not allowed.',
}

const passwordChangeTypes = {
  elev: 'elev',
  mama: 'mama',
  tata: 'tata',
}

// defined minimum role required to perform a CF
const actionPermissions = {
  HOOK: -1,
  STUDENT: 0,
  PARENT: 1,
  TEACHER: 2,
  CLASSMASTER: 3,
  SECRETARY: 4,
  ADMIN: 5,
}

module.exports = {
  actionPermissions,
  actions,
  errors,
  fields,
  getOptions,
  masterObj,
  notificationClasses,
  passwordChangeTypes,
  primaryRoleNames,
  quizItemTypes,
  quizStatuses,
  tableNames,
}
