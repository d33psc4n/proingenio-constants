const chatFields = {
  lastMessage: 'lastMessage',
  firstUser: 'firstUser',
  secondUser: 'secondUser',
  group: 'group',
  nrOfMessages: 'nrOfMessages',
};

const unreadChatFields = {
  count: 'count',
  target: 'target',
  chat: 'chat',
};

const messageFields = {
  chat: 'chat',
  fromUser: 'fromUser',
  content: 'content',
};

const chatActions = {
  getContacts: 'getContacts',
  getMessages: 'getMessages',
  getChats: 'getChats',
  markChatAsRead: 'markChatAsRead',
  sendMessage: 'sendMessage',
};

module.exports = {
  chatFields,
  unreadChatFields,
  messageFields,
  chatActions,
};
