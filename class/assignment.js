const assignmentFields = {
  dueDate: 'dueDate',
  course: 'course',
  title: 'title',
  content: 'content',
  fileUploads: 'fileUploads',
  post: 'post',
}

const assignmentFeedbackFields = {
  author: 'author',
  content: 'content',
  submission: 'submission',
}

const assignmentSubmissionFields = {
  grade: 'grade',
  fileUploads: 'fileUploads',
  author: 'author',
  verified: 'verified',
  assignment: 'assignment',
}

const assignmentActions = {
  postAssignment: 'postAssignment',
  deleteAssignment: 'deleteAssignment',
  addFilesToAssignmentSubmission: 'addFilesToAssignmentSubmission',
  extendDeadline: 'extendDeadline',
  deleteAssignmentSubmission: 'deleteAssignmentSubmission',
  gradeAssignmentSubmission: 'gradeAssignmentSubmission',
  postAssignmentFeedback: 'postAssignmentFeedback',
  postAssignmentFeedbackFile: 'postAssignmentFeedbackFile',
}

module.exports = {
  assignmentFields,
  assignmentSubmissionFields,
  assignmentFeedbackFields,
  assignmentActions,
}
