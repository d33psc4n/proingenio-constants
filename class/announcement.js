const announcementFields = {
  title: 'title',
  content: 'content',
  author: 'author',
  target: 'target',
};

const announcementActions = {
  postAnnouncement: 'postAnnouncement',
  deleteAnnouncement: 'deleteAnnouncement',
  getAllAnnouncements: 'getAllAnnouncements',
};

module.exports = {
  announcementFields,
  announcementActions,
};
