const timetableFields = {
  items: 'items',
};
const timetableItemFields = {
  timeStart: 'timeStart',
  timeEnd: 'timeEnd',
  course: 'course',
  day: 'day',
};

const timetableActions = {
  getTimetableItems: 'getTimetableItems',
  getTimetableItemsAdmin: 'getTimetableItemsAdmin',
  getCurrentLesson: 'getCurrentLesson',
  getCurrentLessonAdmin: 'getCurrentLessonAdmin',
};

module.exports = {
  timetableFields,
  timetableItemFields,
  timetableActions,
};
