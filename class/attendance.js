const attendanceFields = {
  date: 'date',
  semester: 'semester',
  course: 'course',
  student: 'student',
  justified: 'justified',
};

const attendanceActions = {
  postAttendance: 'postAttendance',
  changeAttendance: 'changeAttendance',
  getAllAttendances: 'getAllAttendances',
  getAllAttendancesAdmin: 'getAllAttendancesAdmin'
};

module.exports = {
  attendanceFields,
  attendanceActions,
};
