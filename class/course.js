const subjectFields = {
  name: 'name',
  countsFinalGrade: 'countsFinalGrade',
  hasFeedback: 'hasFeedback',
  hasGrades: 'hasGrades',
  hasAttendances: 'hasAttendances'
}

const courseFields = {
  students: 'students',
  work: 'classWork',
  teacher: 'teacher',
  subject: 'subject',
  classRoom: 'classRoom',
  fileUploads: 'fileUploads',
  whatsapp: 'whatsapp',
  group: 'group',
}

const classWorkFields = {
  assignment: 'assignment',
  quiz: 'quiz',
  attributed: 'attributed',
  graded: 'graded',
  sent: 'sent',
  ongoing: 'ongoing',
  finished: 'finished',
  date: 'date',
}

const courseActions = {
  getTeachers: 'getTeachers',
  deleteCourse: 'deleteCourse',
  addCourse: 'addCourse',
  setCourseExcludedUsers: 'setCourseExcludedUsers',
  changeCourseTeacher: 'changeCourseTeacher',
  addFilesToCourse: 'addFilesToCourse',
  getClassroomName: 'getCourseClassroom',
  getCourseClassrooms: 'getCourseClassrooms',
  removeFilesFromCourse: 'removeFilesFromCourse',
  changeWhatsAppLink: 'changeWhatsAppLink',
}

module.exports = {
  subjectFields,
  courseFields,
  classWorkFields,
  courseActions,
}
