const postFields = {
  author: 'author',
  course: 'course',
  content: 'content',
  fileUploads: 'fileUploads',
};

const postActions = {
  addPost: 'addPost',
  deletePost: 'deletePost',
};

module.exports = {
  postFields,
  postActions,
};
