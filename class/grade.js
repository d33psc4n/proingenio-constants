const gradeFields = {
  date: 'date',
  semester: 'semester',
  grade: 'grade',
  type: 'type',
  course: 'course',
  student: 'student',
}

const averageOverrideFields = {
  semester: 'semester',
  grade: 'grade',
  course: 'course',
  student: 'student'
}

const gradeTypeFields = {
  name: 'name',
}

const gradeActions = {
  postGrade: 'postGrade',
  deleteGrade: 'deleteGrade',
  addEditAverageOverride: 'addEditAverageOverride',
  getAllGrades: 'getAllGrades',
  getAllGradesAdmin: 'getAllGradesAdmin',
  getAllOverrides: 'getAllOverrides',
  getAllOverridesAdmin: 'getAllOverridesAdmin'
}

module.exports = {
  gradeFields,
  gradeTypeFields,
  averageOverrideFields,
  gradeActions,
}
