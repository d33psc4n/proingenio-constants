const courseFeedbackFields = {
  course: 'course',
  feedback: 'feedback',
  student: 'student',
  semester: 'semester'
}

const courseFeedbackActions = {
  addEditCourseFeedback: 'addEditCourseFeedback',
  getCourseFeedbacks: 'getCourseFeedbacks',
  getCourseFeedbacksAdmin: 'getCourseFeedbacksAdmin'
}

module.exports = {
  courseFeedbackActions,
  courseFeedbackFields,
}
