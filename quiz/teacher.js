const quizFields = {
  questions: 'questions',
  nrOfQuestions: 'nrOfQuestions',
  startDate: 'startDate',
  maxPoints: 'maxPoints',
  title: 'title',
  course: 'course',
  requiresFiles: 'requiresFiles',
  timeToFinish: 'timeToFinish',
  description: 'description',
  expirationDate: 'expirationDate',
  onePerPage: 'onePerPage',
  seeCorrectAnswers: 'seeCorrectAnswers',
  active: 'active',
};

const quizStatusFields = {
  status: 'status',
  displayName: 'displayName'
}

const quizBlankItemFields = {
  correctAnswer: 'correctAnswer',
  points: 'points',
  name: 'name',
  quizItem: 'quizItem',
};

const quizChoiceOptionFields = {
  content: 'content',
  quizItem: 'quizItem',
  isCorrect: 'isCorrect',
};

const quizDropdownItemFields = {
  quizItem: 'quizItem',
  choices: 'choices',
  name: 'name',
  correctIndex: 'correctIndex',
  points: 'points',
};

const quizItemFields = {
  blankItems: 'blankItems',
  multipleChoices: 'multipleChoices',
  question: 'question',
  type: 'type',
  dropdownItems: 'dropdownItems',
  fileUploads: 'fileUploads',
  partialPoints: 'partialPoints',
  // THIS MEANS SHOW AS RADIO OR NOT
  showIfOnlyOneChoice: 'showIfOnlyOneChoice',
  content: 'content',
};

const quizItemTypeFields = {
  type: 'type',
  displayName: 'displayName',
};

const quizQuestionFields = {
  items: 'items',
  order: 'order',
  points: 'points',
  quiz: 'quiz',
};

const quizTeacherActions = {
  addEditQuiz: 'addEditQuiz',
  deleteQuiz: 'deleteQuiz',
  gradeQuiz: 'gradeQuiz',
  changeQuizActiveStatus: 'changeQuizActiveStatus',
  gradeQuizManualQuestion: 'gradeQuizManualQuestion',
  postQuizTakeFeedbackFile: 'postQuizTakeFeedbackFile'
};

const quizItemTypes = {
  upload: 'upload',
  fill: 'fill',
  choice: 'choice',
  text: 'text'
}

const quizStatuses = {
  partialGrade: 'partialGrade',
  graded: 'graded',
 }

module.exports = {
  quizFields,
  quizBlankItemFields,
  quizChoiceOptionFields,
  quizDropdownItemFields,
  quizItemFields,
  quizStatuses,
  quizItemTypeFields,
  quizStatusFields,
  quizQuestionFields,
  quizTeacherActions,
  quizItemTypes
};
