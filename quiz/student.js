const quizBlankAnswerFields = {
  quizItem: 'quizItem',
  answers: 'answers',
};

const quizDropdownAnswerFields = {
  quizItem: 'quizItem',
  answers: 'answers',
};

const quizTextAnswerFields = {
  quizItem: 'quizItem',
  content: 'content',
  points: 'points',
  isGraded: 'isGraded',
};

const quizTakeFields = {
  blankAnswers: 'blankAnswers',
  dropdownAnswers: 'dropdownAnswers',
  currentItem: 'currentItem',
  isFinished: 'isFinished',
  user: 'user',
  startDate: 'startDate',
  status: 'status',
  uploadAnswers: 'uploadAnswers',
  totalPoints: 'totalPoints',
  selectedItems: 'selectedItems',
  textAnswers: 'textAnswers',
  gradedQuestions: 'gradedQuestions',
  choiceAnswers: 'choiceAnswers',
  quiz: 'quiz',
  fileUploads: 'fileUploads',
  viewedQuestions: 'viewedQuestions'
};

const quizTakeIncludes = ['currentItem', 'currentItem.type', 'user', 'status', 'quiz', 'currentItem.question'];

const quizUploadAnswerFields = {
  quizItem: 'quizItem',
  points: 'points',
  isGraded: 'isGraded',
};

const quizStudentActions = {
  submitAnswer: 'submitAnswer',
  getCurrentQuestionItem: 'getCurrentQuestionItem',
  getAllQuestions: 'getAllQuestions',
  submitQuizTakeFiles: 'submitQuizTakeFiles',
  getQuizPoints: 'getQuizPoints',
  submitAllQuestions: 'submitAllQuestions'
};

module.exports = {
  quizBlankAnswerFields,
  quizDropdownAnswerFields,
  quizTakeFields,
  quizTakeIncludes,
  quizTextAnswerFields,
  quizUploadAnswerFields,
  quizStudentActions,
};
