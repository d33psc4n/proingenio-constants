const notificationFields = {
  target: 'target',
  title: 'title',
  content: 'content',
  author: 'author',
  route: 'route',
  actionObject: 'actionObject',
};

const fcmTokenFields = {
  token: 'token',
  user: 'user',
  identifier: 'identifier',
  deviceInfo: 'deviceInfo'
}

const notificationClassFields = {
  name: 'name',
  displayName: 'displayName',
};

const notificationClasses = {
  chat: 'Chat',
};

const notificationRouteFields = {
  name: 'name',
};

const notificationActions = {
  deleteAllNotifications: 'deleteAllNotifications',
  deleteNotification: 'deleteNotification',
  addFCMToken: 'addFCMToken',
  deleteFCMToken: 'deleteFCMToken'
};

module.exports = {
  notificationClasses,
  fcmTokenFields,
  notificationClassFields,
  notificationFields,
  notificationRouteFields,
  notificationActions,
};
