const schoolFields = {
  school: 'school',
  globalGroup: 'globalGroup',
  displayName: 'displayName',
  platformDomain: 'platformDomain',
  contactPhoneNumber: 'contactPhoneNumber',
  hasLogo: 'hasLogo'
}

const schoolActions = {
  createSchool: 'createSchool',
  seedSchool: 'seedSchool'
}

module.exports = { schoolActions, schoolFields }
