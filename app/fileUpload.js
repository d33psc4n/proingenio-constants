const fileUploadFields = {
  author: 'author',
  name: 'name',
  file: 'file',
  metadata: 'metadata'
}

const fileMetadataFields = {
  fileName: 'fileName',
  metadata: 'metadata'
}

const fileUploadActions = { deleteFileUpload: 'deleteFileUpload' }

module.exports = { fileUploadFields, fileUploadActions, fileMetadataFields }
