const userFields = {
  firstName: 'firstname',
  lastName: 'lastname',
  avatar: 'avatar',
  phoneNumber: 'phoneNumber',
  notificationClasses: 'notificationClasses',
  notificationEmail: 'notificationEmail',
  notificationInstant: 'notificationInstant',
  language: 'language',
  username: 'username',
  password: 'password',
  birthDate: 'birthDate',
  cnp: 'cnp',
  email: 'email',
  gender: 'gender',
  children: 'children',
  classRoom: 'classRoom',
  defaultPassword: 'defaultPassword',
};

const userActions = {
  editUser: 'editUser',
  changeAvatar: 'changeAvatar',
  deleteUser: 'deleteUser',
  createUser: 'createUser',
  setNotificationSettings: 'setNotificationSettings',
  setUserGroups: 'setUserGroups',
  getUsersInGroup: 'getUsersInGroup',
  getUsersNotInGroup: 'getUsersNotInGroup',
  getUsersForGroups: 'getUsersForGroups',
  addRemoveUsersToGroup: 'addRemoveUsersToGroup',
  getAllUserGroups: 'getAllUserGroups',
  updateAllUserNotifications: 'updateAllUserNotifications',
  editProfile: 'editProfile',
  getAllUsers: 'getAllUsers',
  changePassword: 'changePassword',
  generateUserCard: 'generateUserCard',
  changePasswordAdmin: 'changePasswordAdmin'
};

module.exports = {
  userFields,
  userActions,
};
