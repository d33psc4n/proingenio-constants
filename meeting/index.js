const roomFields = {
  name: 'name',
  group: 'group',
  nrOfPeers: 'nrOfPeers',
  server: 'server',
  hostname: 'hostname',
};

const meetingServerFields = {
  hostname: 'hostname',
  bandwidth: 'bandwidth',
  cores: 'cores',
  url: 'url',
  isReachable: 'isReachable',
  isActive: 'isActive',
  port: 'port',
};

const zoomRoomFields = {
  meetingUrl: 'meetingUrl',
  passcode: 'passcode',
  group: 'group',
  closeAfter: 'closeAfter',
  name: 'name',
};

const meetingActions = {
  createRoom: 'createRoom',
  createZoomRoom: 'createZoomRoom',
  closeZoomRoom: 'closeZoomRoom',
  getBestServer: 'getBestServer',
  closeZoomRoomAdmin: 'closeZoomRoomAdmin',
  getAllZoomMeetings: 'getAllZoomMeetings',
  closeRoom: 'closeRoom',
  getAllMeetings: 'getAllMeetings',
};

module.exports = {
  roomFields,
  meetingServerFields,
  zoomRoomFields,
  meetingActions,
};
